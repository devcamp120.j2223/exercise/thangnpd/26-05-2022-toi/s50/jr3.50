const gBASE_URL = "https://devcamp-student.herokuapp.com/users/";


function onBtnActionClick(paramAction) {
  if (paramAction == 'back') {
    window.location.href = "index.html";
  } else {
    createNewUser();
  }
}

function createNewUser() {
  //B1: thu thập dữ liệu
  var vObjectRequestData = {
    name: $("#fullname").val().trim(),
    email: $("#email").val().trim(),
    password: "123456",
    phone: $("#phone").val().trim(),
    birthday: $("#birthday").val().trim(),
    gender: "Female",
  };

  //B2: kiểm tra dữ liệu
  var vCheck = validateData(vObjectRequestData);

  //B3: Xử lý dữ liệu
  if(vCheck) {
    callAjaxCreateNewUser(vObjectRequestData);
  }
}

function validateData(paramObj) {
  var vResult = true;
  if(paramObj.name == "") {
    alert("Chưa nhập họ tên");
    vResult = false
  }
  if(paramObj.email == "") {
    alert("Chưa nhập email");
    vResult = false
  }
  if(!paramObj.email.includes("@")) {
    alert("Email ko có @");
    vResult = false
  }
  if(paramObj.phone == "") {
    alert("Chưa nhập phone");
    vResult = false
  }
  return vResult;
}

function callAjaxCreateNewUser(paramObj) {
  $.ajax({
    url: gBASE_URL,
    type: "POST",
    dataType: "json",
    data: JSON.stringify(paramObj),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: function (data) {
      alert("Tạo thành công user có id: " + data.id);
      window.location.href = "index.html";
    }
  });
}

$(document).ready(function () {
  $(".btn-back").on('click', function () {
    onBtnActionClick('back');
  });

  $(".btn-create").on('click', function () {
    onBtnActionClick('create');
  });

});